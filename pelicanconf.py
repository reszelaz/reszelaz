AUTHOR = 'Zbigniew Reszela'
SITENAME = 'Zbigniew Reszela\'s Space'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('ALBA Synchrotron', 'https://www.albasynchrotron.es/en'),
         ('Sardana', 'https://sardana-controls.org/'),)

# Social widget
SOCIAL = (('LinkedIn', 'https://www.linkedin.com/in/zbigniew-reszela-177a527/'),
          ('GitLab', 'https://gitlab.com/reszelaz'),
          ('GitHub', 'https://github.com/reszelaz'),
          ('Twitter', 'https://twitter.com/reszelaz'))

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
