Title: About me
Slug: aboutme
page-order: 2

Hi, 

I am Zbigniew Reszela, a polish living in Barcelona.
I am a husband and father of two daughters.
I develop my professional career in international
scientific environment where I work with controls software and hardware
for almost 15 years of which the last 10 years
as group responsible.

Everyday I leverage Python programming skills and contribute to Open Source
software projects used in European scientific installations.
I love to explore new technologies and processes,
especially those from the DevOps field.

I am Agile enthusiast and I constantly promote agile methods in my 
organization. Always focused on delivering value to the user 
in the shortest possible way from requirements to operation.

Inspired by Michael Jordan, who once said: "Talent wins games, but teamwork
and intelligence wins championships" I consider myself a team player who loves
to learn from others and to share with others.

Hope you will find something interesting on this site.

Cheers!
