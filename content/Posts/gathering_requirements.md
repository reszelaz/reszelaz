Title: About Gathering Requirements
Date: 2022-09-18

# Why is gathering requirements so important?

Requirements in the IT/ Tech projects are often called "business" requirements as they present to software developers the business side of the project. They are extremely important as they create understanding and bring closer both sides. Without well defined requirements projects simply can not be successful.

When you prepare requirements you must forget about being perfect, and focus on being right. Later on, during project execution, you can and should apply corrections and refinements as the business and situation changes.

In an Agile environment you start with a basic understanding of the requirements and then you refine it adding details to the use cases and user stories in a continuous and iterative manner.

In waterfall methodology the requirements gathering process  is more formal and controlled and it aims to close a stage beyond which requirements changes are more difficult to apply.

But in any methodology, there is an initial effort whose purpose is to get a better understanding, scope and sense for how the project needs to develop.


# One Man Band vs. Software Development Life Cycle Roles

Whether you form part of a bigger organisation, with clearly separated roles e.g. Business Analyst, Solutions Architect or Development Leader, etc. or you play all these roles simultaneously you will participate in different project activities like Project Scoping, Requirements Gathering, Design, Development and Testing during which you will constantly recur to the list of your Project Requirements.

In the scientific environment that I work on a daily basis, we - Controls Engineers are in charge of gathering requirements but we could not get the job done without the Subject Matter Experts - the Scientists who provide us with the information on what is needed to be built (we may refer to them as the Business side of the project, as they are our Internal Stakeholders). But still, in our case we clearly operate as a One Man Band with duties that span over the whole software development lifecycle: from business analysis up to users training.

In any case, it is a Shared Responsibility of all to ensure a Mutual Respect by maintaining an open dialog during which any ideas can be freely expressed and making everyone feel as a necessary part of the Process/ Team.

# Tools

There are numerous tools you should know when working on Requirements, and I find that the following are particularly easy to apply and bring a lot of benefits.

- Lexicon - Defining a common Lexicon with a set of Vocabulary and Definitions as well as avoiding jargon improves mutual understanding.

- Persona - is a fictional representation of a group or class of the users. Your imagination is the limit when defining Personas, you can give them names, depict them with photos and think up a backstory. The purpose of using Persona is to make easier  the communication between all the people working on the Requirements and enhance our empathy as we can imagine better a particular user

- Use Cases and User Stories - both refer to, in my opinion, the most important dimension of the Requirements - the User Requirements, and describe what the user needs from the solution to be able to perform/ execute/ deliver. User Stories are short descriptions of **who?** and **why?** needs a given capability. They focus on the outcomes and benefits the user will achieve and not on the exact description on how the system will be used by the user, hence they lack a lot of technical details. Work on the User Stories starts from writing a sentence following the pattern: *As a <role\> I want <capability\>, so that <receive benefit\>*.
On the other hand the Use Cases are rather a longer descriptions on **how?** the user will interact with the system, often describing step-by-step the process flow with the exception and alternative paths. In my opinion, the main difference between these two is that the User Stories are outcome/ benefit oriented while the Use Cases are process oriented. Also, Use Cases are a detailed guideline for Developers so they could use them to directly work on the solution, while the User Stories are more a reminder that a detailed analysis of the requirements still needs to be performed. In Agile environments this analysis will be done between the Development Team and the Product Owner who is the link between the Business and the Developers.

- Wireframes and Storyboards - Low fidelity wireframes are intentionally not sophisticated block representation of the user interface. With a very low cost of development, it can be as simple as a sketch on a whiteboard, and should provide great benefits in identifying missing requirements. High fidelity wireframes are usually prepared as a next step toward a prototype with higher level of detail of a solution. Storyboards are collections of wireframes designed to show lines of actions. Their main role is to ensure that the use cases are possible. While a prototype is an already clickable/ usable wireframe allowing users to experience user flow.

- Prototype and Spikes - are disposal proof-of-concept partial works which aim to evaluate the design or technology (Spikes) decisions.
    
# Meeting Skills and Techniques
    
Meetings are one of the most efficient ways of gathering requirements. In one-on-one meetings you could choose to actively ask questions or simply observe what is being done by the other side and this way learn what is needed.

 On Focus Groups you could get a more detailed view of the requirements especially when more people participate. On Focus Groups it is a good idea to split the facilitator role, who should focus on guiding participants and ensuring the discussion stays within the scope, with the scribe role.

There are some skills and techniques that are essential for running good meetings. 

- As a good facilitator you must prepare a Meeting Agenda and a set of Objectives that ideally should be shared with the participants beforehand. For keeping the meeting on the right track you can not let the participants dedicate time to out of scope discussion. You should quickly identify them and kindly ask to postpone them until the right time.
- Dedicate sufficient time to explain the project Goals  - what we want to accomplish with the project. This will help you to get people on board and engaged. Reminding the Goals frequently will avoid conflicts due to misunderstanding.
- For interviewing people you may find two techniques very useful: Active Listening and 5 Whys. In the first one, from time to time, you paraphrase or repeat back what the other side said employing a question at the end e.g. Is that right? Did I understand it well? In this communication technique you make the other person feel that it is being heard and understood, which makes the other side open even more and expose more details. In the second one, after hearing a requirement you start a sequence of iterative questioning "Why you need that?". Your questions must be expressed in the curiosity tone and can not sound like a judgement.
- Early identify when the people need a Break, either because of the loss of focus, or because of some problem escalation and call for a quick ending.

# Questionnaire

Unfortunately in-person Meetings are not always possible, for example when the stakeholders are remotely dispersed. Virtual meetings may also be hard to organise due to time differences, in this case Questionnaires come to rescue. Here are some hints on how to get the best out of the questionnaire:

- When preparing the content of the questionnaire, the first thing you need to know is who you want to survey. If there are multiple target groups, then I recommend you to prepare a different set of questions for each of them instead of trying to combine all of them in one. 
- Surveys should not take too long to fill out, I would say that 5-15 min is a reasonable choice. In some extreme cases you may decide to go beyond that, but I would say that exceeding a half an hour period is not a good idea. Too long ones will make the respondent lose the attention while filling out.
- Use dynamic paths and ask questions for extra details only to those respondents who may potentially know the answer, this will reduce the fatigue in those who do not know the details.
- In evaluative questions, use objective description of the answer e.g. "In scale from 1 to 10 indicate how much you like this or that, where 1 is you don´t like at all and 10 is you love it".
- And last but not least, leave open questions for the respondents who would like to add some extra information you have not foreseen.

# Tips and tricks

A simple hint on how to not forget about any critical requirement is to iterate over the following aspects that almost certainly are present in your business

- What Data is involved in the process, where does it come from and where it is supposed to go, who and how uses it? 
- What are the exact Processes that need to happen? And what are their steps? 
- What are the Interfaces or Dependencies with other systems that our solution will need to interact with? 
- What Quality criteria needs to be met and what are the acceptable errors. 
- What level of Performance and Scalability the system will need to ensure.
- What Security measures need to be in place to prevent security breaches?

Your requirements will need to be prioritised, and usually you want someone to make the prioritisation for you. I like the most the following two prioritisation techniques:

- Rank Ordering consists of comparing pairs of requirements and deciding which is more important. This technique fits better for a small or medium list of requirements.
- 100 dollars: You must imagine you have 100 dollars and can spend it on different features (requirements) - you decide the proportions. 

Requirements typically specify the normal behaviour and lack to specify how the solution should behave when something goes wrong. Questioning the *Happy Paths* is the best thing you can do: "What happens when something goes wrong?", "What is the worst case scenario?"

Requirements concerning the *Power User Features* are usually well defined thanks to the great collaboration with these stakeholders who typically are deeply involved in the project, know the shortcuts and exploit the most complex business models. However their requirements almost for sure will represent a small percentage of the users. These requirements in most of the cases should end up prioritised lower than the regular user requirements.


# Writing Down Requirements

Requirements, while they are being gathered, must be put down on paper for further reference. It does not matter if you write a text based explanation of the need in form Requirements Document, or a more specific and complete register of all requirements in form of Requirements Specification, the below quick tips will make your writing successful:

- It is better to start talking about the most critical things, and continue with those of reduced importance e.g. Must -> Should -> Could.
- Your biggest challenge, as in any other example of technical writing, will be to achieve clarity and conciseness at the same time. Conciseness encourages readers to stay with the document and clarity ensures that the message is conveyed. 
- You should continue using the Lexicon established at the beginning of your Requirements Gathering adventure and avoid introducing new vocabulary. 
- Avoid the tendency to write about "How?" and focus on "What? and "Why?". This way you won't mix Requirements with Architectural Design.
- Organise related requirements in groups and give them unique ID to facilitate reading and following references.
- Remember: active voice makes it clear who has to do what by when and using passive voice minimises the impact of a statement.
- Avoid imprecise words which lead to misunderstandings, some examples are: Acceptable, Best, Fast, Robust, Probable or Minimise.

# Wrap-up

Gathering Requirements activity is an extremely important part of the Software Development Lifecycle but is frequently underestimated, especially by young professionals. If you pay enough attention and do it well, then chances are that your project will turn out to be a success.. 

I shared with you different requirements techniques and activities I learned and applied in different projects. 
I recommend you to take a look on some examples of the requirements from the [Sardana Configuration Tool - SEP20](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749) document or the [Sardana Users Questionnaire](https://gitlab.com/sardana-org/sardana-questionnaire) both of them prepared together with the Sardana project Community.
