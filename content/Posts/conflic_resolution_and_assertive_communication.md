Title: Conflict Resolution and Assertive Communication
Date: 2022-12-19

Conflicts are differences in viewpoints including two or more sides that affect their relationship. Conflicts are a normal part of any human interaction and you will certainly find them on the way of your life, including in the workplace. Unattended conflicts tend to escalate to a critical situations. It is very important to identify them and solve them as early as possible.

This post is mixture of theory about conflicts and assertive communication acquired on various courses and personal examples I experienced or witnessed at my workplace.

The courses I talk about are "Effective Communication and Conflict Management" by Alberto Ibai and "Conflict Resolution" by Paco Molinero (both presential, organized at my workplace) and "Conflict Resolution in the Workplace" by Stephen Haunts (online - Pluralsight platform). I highly recommend you these sources of information.

# Conflict definition and causes

A conflict situation appears when there is a relationship of interdependence between people with different objectives or points of view and it may affect the qualities of the interaction. Conflicts are a normal part of any human interaction, were always present in society, and will certainly appear in your workplace. Your organization needs to contain the conflict as early as possible. Typically conflicts are caused by:

- **conflicting resources** - more than one person or group competes for a shared resource e.g. budget allocations between departments.
- **conflicting styles** - people with different personalities or work habits have to collaborate e.g. you prefer a clear and transparent organization and your colleague feels better within an anarchic environment.
- **conflicting perceptions** - certain information did not reach everyone causing people different perceptions e.g. you get information from a different department that affects your perception about the future of your project, or your boss's vision was not communicated to all team members causing conflicting opinions.
- **conflicting goals** - conflicting goals emerge in your organization e.g. your Product Owner transmits your current sprint goals and your manager comes with conflicting directives.
- **conflicting pressures** - similar to conflicting goals, but affecting short-term tasks e.g. two different incidents that according to the affected stakeholders are of equal priority.
- **conflicting roles** - either we have to perform a task that's outside our normal role or responsibilities that may be interpreted as stepping into someone else's territory or we believe that the task that we were just assigned should be a responsibility of someone else e.g. you proactively step into your colleagues project to solve a minor bug you just hit, and you discover and report a major technical issue.
- **difference of personal values** - you were assigned a task that conflicts with your ethical standards and what you believe in e.g. you are forced to use a commercial tool while you share Free Open Source Sofware philosophy knowing that FOSS equivalent exists.


# Impact of a conflict

Conflicts have consequences in different areas of our life. 

The impact on **communication** and **project** areas is especially harmful to your work organization because it tends to form a chain of bad consequences. Conflicts at the initial stage can lead to people not wanting to talk to each other impeding the flow of vital information. This miscommunication then may have a consequence on the quality of work results and cause a higher rate of defects.

The impact on **psychological** and **relationships** areas starts with the increase of stress, first on the parts of the conflict, but not only. The team atmosphere suffers when there are some individuals not getting well. This leads people to reduce team collaboration and problem-solving.

Poor quality and reduced collaboration are serious threads for your projects.  They may introduce delays and eventually will cause missed deadlines.

# Escalation of a conflict

Difficulties in relationships due to different points of view or simply discomfort about a given situation are one of the first symptoms of a conflict. 

Then appear first unproductive and disappointing short confrontations when parties compete for who is stronger and the objectives and reasons move to the second plan. This makes you feel irritated and upset.

After several such incidents, you start thinking that talking is useless and begin to act without asking the other party. With less communication, we misinterpret facts and motives. Since mutual dependencies are blocking the parties they choose unilateral solutions. Your thoughts keep returning to the problem.

The atmosphere is tense so much so that the interaction frequently turns into attacks on the reputation and image of other parties. The specific issue is no longer important, now it is important to who will and who will lose. This situation is now your constant worry and concern.

With time the conflict turns into a crisis. Interactions turn into mutual disqualifications with repeated and deliberate offenses. Parties enter into the destructive strategy and both accept to lose. You start accepting thoughts about a rupture in a relationship, leaving a job, and using violence, ...

These stages demonstrate that an unattended conflict can quickly go out of control and end up in a crisis with appalling consequences. You should try to stop the escalation process at its earliest possible phase. Many times you ask yourself the question: Why it's me who should care about solving the conflict and not the other party? The first thing you must remember is that your point of view about the conflicting situation is just your interpretation. By hearing the other person's point of view you may learn new facts which may change your attitude toward the situation. Finally making the first step in conflict resolution is a way to win. Why? Take a look at the crisis stage - in the end, both parties lose. So, procure not to reach so far and you will win!

# How to face a conflict?

Your attitude at the early stages of the conflict is crucial for its resolution. The higher it escalates the more complex it will be to unwind it. The best is to early recognize that there is a problem and that it needs to be attended to instead of denying its existence. Then, go to the other party and make it aware of your recognition and try to make it recognize it as well. This will prepare the other party for receiving your future attempts at resolution and increments the chances that the other party work on the resolution as well. As a next step, all parties need to express their objectives and concerns. Here, listen carefully without interrupting the counterpart and communicate politely your worries. When the key points are identified you can start looking for a resolution. Decide together on corrective actions and start to employ them. Hopefully, they will lead you to a successful resolution of the conflict.

# Conflict Resolution Styles

People react to conflicts differently, we learn this throughout our whole life and we develop our style.
When choosing the strategy to manage a conflict we mainly care about the: 

1. Achieving our personal goals: What and how important is it for us?
2. Maintaining the relationship: Will I maintain the relationship with the other party, or not, after the conflict, and how important it is?
 
There exist five main styles of conflict management:

**Withdrawal**. You try to stay away from conflict and conflicting people instead of facing them. You give up your personal goals in favor of calm and tranquility. The advantage of withdrawing is it may help to maintain relationships that would be hurt by resolving the conflict. The disadvantage is that conflicts remain unresolved and that the other party may finally use it to walk over you. But still, the withdrawal style is good when:

- the issue is trivial
- there is little chance of satisfying your wants

**I win, you lose**. You try to impose your solution at all costs and prove the other person is wrong. The priority is your goals, not the relationship. You try to win by attacking and in extreme cases, you don't mind getting violent. You assume that conflict ends when you win and the other party loses. When the relationship must be maintained the eventual initial winning may turn out to be misleading cause the losing party may revolt sooner or later. 
The advantage is that when this decision is correct, a better decision without compromise can result. The disadvantage is that it may breed hostility and resentment towards the person using it. But still, this strategy is good when:

- conflict involves personal differences that are different to change
- others are likely to take advantage of noncompetitive behavior
- the decision is vital in a crisis
- when unpopular decisions need to be implemented
    
**I lose, you win.**. You tend to give up in a conflicting situation when you desperately look to maintain a good relationship or look for acceptance and you don't care so much about your objectives. You are afraid that someone will get hurt and that will ruin the relationship so you accept whatever mandates the other party. The advantage of this strategy is that by being accommodating you can help maintain a relationship. The disadvantage is that giving in may not be productive and the other side may be taken advantage of. Use it when:

- Maintaining the relationship outweighs other considerations
- Suggestions and changes are not important to the accommodator
- Minimizing losses in situations where outmatched or losing

**Compromise**. You care equally about your objectives and the relationship and you would accept lowering your winnings if the other party does the same. The advantage of this strategy is that relationships are maintained and conflicts are removed. The disadvantage is that compromise may create less-than-ideal outcomes. Use it when:

- important and complex issues leave no clear or simple solutions
- when all conflicting people are equal in power and have strong interests in different solutions

**Cooperative: I win, you win**. You highly value your own goals and relationships. You see conflicts as an opportunity and not as a thread. You intelligently look for achieving your and the other party's objectives in an accommodating way ensuring that there are no negative feelings and no stress. The advantage of this strategy is that both sides get what they want and the negative feelings are eliminated. The disadvantage is that it takes a great deal of time and effort to resolve the conflicts. Use it when: 

- peer conflict is involved, when trying to gain commitment through consensus building
- learning and trying to merge difficult perspectives

The following graph put together these conflict resolution strategies and places them on the objectives axes.

![Conflict Resolution Strategies]({static}/images/conflict_resolution_strategies.png)

Here I share with you a simple rule of thumb that will help you to choose the correct strategy.

- Cooperative and compromise are good when there is no time restriction while I win, you lose and I lose, you win are good for urgent situations
- Cooperative, I lose, you win and Withdrawal are good for maintaining the relationship with the following differences:
    - Cooperative will even strengthen it
    - I lose, you win comes with the risk of the other party trying to take advantage of you in the future conflicts
    - Withdrawal does not resolve the conflict

# Methods for Conflict Resolution

## Reflexive (Active) Listening

Requires from us a total focus on listening to the speaker. We analyze the message and give back short reflections (simply repeating what we heard or paraphrasing). When listening remember to pay attention to non-verbal messages like body language, voice level, etc. Reflexive listening is not possible without empathy - the ability to understand and share the feelings of another, respect and trust so remember to be kind all the time.

Your reflexing feedback should be free of judgments, short and clear. Don't start with "From what you said, I understand that you are angry ..." but use a directly affirmative tone in the second person e.g. "You are angry because ...". Employing Reflective Listening will help the other party to open himself and fully express his feelings and objectives. 

## Nonviolent Communication (NVC)

Is a way of communicating - both speaking and listening, that leads us to give from the heart, connecting with ourselves and with each other. Instead of habitual, automatic reactions, our words become conscious responses based firmly on awareness of what we are perceiving, feeling, and wanting. We are led to express ourselves with clarity and honesty while simultaneously paying others respectful and empathic attention. An NVC intervention consists of four components: 1 - observations, 2 - feelings, 3 - needs, and 4 - requests. 

First, we observe what is happening in a situation e.g. what others are saying or doing that is either enriching or not enriching our life. The trick is to articulate this observation without introducing any judgment or evaluation. When we combine observation with evaluation we decrease the likelihood that others will hear our intended message. Instead, they are apt to hear criticism and thus resist whatever we are saying. 

Next, we state how we feel when we observe this situation: are we angry, lonely, jealous, proud, or happy? Seems an easy step, but is not. We are not taught equally to express our feelings, and there are people having difficulties doing so, especially in the workplace. There are professions, like physicists, computer scientists, etc. which may have extra difficulty because of constant quantifying problems and solutions in their daily work.

Then we connect our needs to the feelings we just identified. When we express our needs, we have a better chance of getting them met. If we don't value our needs, others may not either. We accept full responsibility for our feelings but not the feelings of others while being aware that we can never meet our own needs at the expense of others.

Finally, we follow immediately with a specific request that addresses what we would like from the other person that would enrich our lives. Here we should use positive language when making requests: always ask others what you would like them to do, never what you would like them not to do. Negative requests are likely to provoke resistance. Make clear and concrete requests, vague language contributes to internal confusion.

In a conflicting situation, arrange a private meeting with the other party and try to apply the NVC format. When you’re in an extreme mood, avoid making any promises or even making phone calls, writing letters, or sending emails. Often when you are angry it is tempting to pen a stinging message. That’s the worst time to do that. Instead, wait until things cool off and then try it.

## Assertiveness

This means having equal respect for ourselves and others. It fully acknowledges our rights and responsibilities and those of other people with whom we are dealing. It's about being confident, clear, and concise in stating what you want, but at the same time being prepared to acknowledge the other person's viewpoint and own needs. Thinking assertively will guide you to seek solutions that will benefit all involved. This should give you a win/win resolution. 

## Empathy

Is the ability to understand and share the feelings of other people. Empathy requires you to change your perspective: to see beyond your world, beyond yourself and your concerns. When you employ empathy in conflict resolution you will realize that other people most probably aren't so evil, stubborn, or unreasonable. If you listen carefully to what they say and try to be within their skin, then you will understand their position in the conflict.

You can accept that people have differing opinions from your own and that they may have good reason to hold those opinions. Examine your attitude. Are you more concerned with getting your way, winning, or being right? Or is your priority to find a solution, build relationships, and accept others?

## Wrap-up

Conflicts are differences in viewpoints including two or more sides that affect their relationship. Conflicts are a normal part of any human interaction and you will certainly find them on the way of your life, including in the workplace. Unattended conflicts tend to escalate to a critical situations. It is very important to identify them and solve them as early as possible. People develop different conflict resolution styles which they apply automatically during their life. You should try to select the appropriate style depending on what you would like to achieve in the objectives/relationship field. In a conflicting situation, arrange a private meeting with the other party and listen carefully to the other narration and be empathic, express yourself using NVC, and be assertive. Remember, treat the other party with respect and expect respect for yourself.



