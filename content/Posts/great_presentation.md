Title: How to make a great presentation
Date: 2023-02-28

This article is not based on my own experience. It is entirely based on a great book "Presentation Secrets of Steve Jobs" by Carmine Gallo. I strongly recommend you read this book. It is a great and complete guide on how to give great talks, based on a unique in its kind presentation "guru" - Steve Jobs. 

!["Presentation Secrets of Steve Jobs" by Carmine Gallo]({static}/images/presentation_secrets_of_steve_jobs.jpg)

In continuation, you will find some recommendations from this book, which in my opinion are the first things you should do to improve your presentation skills.

# How to start?

Before you prepare the presentation first you must create a story you would like to tell to the listeners. Much before opening the PowerPoint or similar tool. It is better to take a piece of paper and a pen and sketch your ideas there. This will help you to achieve a clear organization of the story. In this process investigate the matter, consult the experts, organize the ideas, ask your colleagues, and sketch the narration. A good proportion is to dedicate 1/3 of the time you have for working on the presentation to this phase. Once the story is ready, it will be much easier to make the slides and prepare yourself for exposing them.

Let's start with listing nine pillars of a great presentation that Carmine Gallo identified in every great presentation of Steve Jobs:

1. Heading
   
    A short message (140 words or less), easy to remember and written in the form: subject - predicate - complement.

2. Declaration of emotional engagement
   
    Try to finish this sentence: This product (company, endeavor, function, ...) evokes my enthusiasm because ...

3. Three main thoughts
   
    Your listeners are capable of remembering only 3-4 main thoughts.

4. Metaphors and analogies
   
    Using Metaphors and analogies make your talks especially interesting.

    Metaphor - The use of a word or phrase to refer to something other than its literal meaning, invoking an implicit similarity between the thing described and what is denoted by the word or phrase. E.g. A computer is like a bike for our brains.
  
    Analogy - A relationship of resemblance or equivalence between two situations, people, or objects, especially when used as a basis for explanation or extrapolation. The microprocessor we produce is like a brain for a computer.

5. Demo
   
    If the product can be demonstrated you should do it during the presentation.

6. Partners
   
    Share the scene with partners that will help you in achieving success.

7. Opinions of clients or third parties
   
    Attach opinions of clients or third parties will help you gain credibility.

8. Videos
   
    Attaching short videos (2-3 min max) will distinguish your presentation from others.

9. Flipcharts, props, etc.
    
    Bring props e.g. the real product and show it.

# What? Story, content, etc.

## What is all about it for me?

The audience wants to be informed, educated, and entertained. But, above all, the audience wants to know: "What is all about it for me?" When we prepare the presentation we must remember that we are not the most important part but it is the audience. 

If our product will help the audience to gain money - we need to tell them that, if it will help them to save money - we need to tell them that, if it will help them in some activity - we need to tell them that. Let's tell it clearly and straightaway, and repeat it!

The audience does not care about the product we offer. They care about themself!

## Share your passion

A leader has in mind a clear vision of how the future will look like. Leaders are fascinated by the future. Leaders demand changes and are deeply unsatisfied with the current situation.

We need to figure out what is our passion. Frequently it is not the product we designed, and produced, but the way it will make life easier for our customers. Look very deep into ourself and find your passion. As soon as you found it share it with the audience. To inspire others you need a passion - messianic enthusiasm to improve the world.

## Catchy slogans

Form catchy slogans - one sentence declaration of the vision and use and repeat them in your presentation. Say the heading even before introducing your product and then continue with its description. At the end repeat the heading. 

Slogans are remembered if they are short, concrete, and give a personal benefit. 

Form your slogans from the point of view of the user - they should answer the question - "What is all about it for me?". Remember the audience is the most important here.

## The rule of three

The rule of three is one of the most important ideas of communication.

Great agendas are composed of three to four parts. More detailed explanations of topics should not exceed three to four points. Descriptions of products contain three to four features. Demonstrations are composed of three to four parts, etc.

When we try to answer a complex question or present something which requires longer explanations, first we should think of a story that we want to tell with clearly distinguishable three to four main points. We list them, then go into details one by one, including facts, examples, analogies, metaphors or jokes from our life, recommendations of third parties, etc. And finally, come back and list these points as a summary. The slides which we use in this presentation should reflect what we currently talk about, there is no need to make it more complicated.

## An enemy vs. a hero

Introducing *an enemy* in your presentation, as a representation of a problem, make the audience stand beyond *a hero* - a representation of a solution. Always maintain this order: first a problem, then a solution. The problem can be simply a description of an impediment that our customers suffer.

When we describe the problem, don't go into details too quickly. Let's start with general ideas and then hierarchically move to go to details. 

When we introduce *a hero* we must immediately cleanly explain the benefits to the customer: "What is all about it for me?"

TV adds the last 30 seconds, and frequently this is enough to present *an enemy* and *a hero*. This proves that it is not time-consuming.

Persistence is mandatory to achieve success. Real persistence originates from a passion. If you are passionate about what you do, about solving a given problem for the customers, then you have greater chances to persist and achieve success.

# How? Slides, demos, etc.

## Simplicity is the highest level of sophistication.

Putting everything on one slide shows the laziness of the author. Putting dense text as well. Simplifying slide contents and removing the less important aspects highlight the important ones.

Placing on a slide content or text which does not correspond to what we say makes it harder for the audience to maintain the focus. But don't put on slides the same text that you will say in your presentation. Avoid bullet points on slides!

Images should be complemented with a small figure/pointer guiding the audience on where to focus.
When you don't use text on slides - ideal, then you must have it very clear what you want to say and be confident in it.
Simplicity applies not only to the slide content but also to what we say.
Let's focus on one aspect only per slide.

## Be careful with numbers

Numbers seldom are understandable if not put in a context familiar to the audience. Analogies and finding similarities between different things are good ways to give context to the numbers. Numbers are just a supplement to the main thought of the presentation. Pay special attention when choosing which numbers to include in the presentation.

## Easy language

If our presentation is confusing and full of technical jargon we lose a chance to get people involved. Our main objective should be to try to be comprehensible. 

Let's try to use simple words and descriptive adjectives. Let's be enthusiastic about our product, if we don't do it we won't make the audience be so. Our words should be simple, concrete, and emotional.

Prepare your speech text. Review it many times and eliminate unnecessary words and jargon. Validate your text with tools like UsingEnglish.com to check its density.

## Share it with other speakers

Sharing presentations with other invited speakers breaks the monotony, similar to performing demos, showing videos, etc. It is good to invite experts, instead of presenting topics in which we are not specialists. This sounds more credible. When you present a product, you could invite clients that were using it who could share some words of satisfaction.

When an expert/client can not be physically present during the presentation, you can always include a video appearance.

Do not forget to acknowledge the work of people who helped you, which can be co-workers, partners, or clients. This will demonstrate your well-doing.

## Live demos

Consider employing live demos in your presentation from the early stage of working on it.
Five hints about making great demos by Guy Kawasaki. Demos should be:

- short - don't bore the audience
- clear - include 1-2 main and easy-to-understand thoughts 
- teasing - show the most interesting functionalities that distinguish it from others
- dynamic - maintain a dynamic pace, make activities of less than 15 s
- concrete - answer how it solves concrete problems

## Highly emotional experience

When time will pass, the audience won't remember all the details, statistical data, etc. of your presentation. They will remember one, at most few, highly emotional events/experiences. Don't reveal a great surprise directly. Gradually build an atmosphere to do it, so in the end, the moment of presenting the surprise will be a highly emotional/experience. Rehearse very well this part of the presentation, so it goes smoothly.

# Make it perfect

## Performance

Three techniques of body language:

1. Maintain eye contact.
   
    Rarely read from slides or notes. This makes you appear sincere and self-confident and builds trust with the audience. To do that you need to know very well what to say on which slide.

2. Maintain an open posture.
   
    Don't cross your arms, and don't hide your hands. Always stand face-to-face with the audience.

3. Use hand gestures.
   
    These will complement your speech and highlight important thoughts. 

Make your voice sound interesting:
Avoid boring, monotonic tones. Modulate your tone: change pace and level, don't rush, and pause when necessary. Use as much content in your presentation so you can make it without rushing and be on time. By modulating your voice and pausing you help the audience capture every word of your talk. Slow down when you come to really important parts of the talk.

Nonverbal communication and the tone of your voice are maybe even more important than the words you choose in your presentation. Be self-confident all the time in your performance.

Rehearse multiple times your presentation. Record it and watch to analyze your body language and voice.

## Practice, practice, and practice...

If you want to sound spontaneous you must rehearse your performance multiple times. Record your rehearsals and watch them to find possible improvements. Invite colleagues to watch them with you and ask for feedback.
Memorize the text of the presentation to avoid reading.
Pay special attention to body language.
Avoid filling your speech with "yyy", "hmmm", etc. it is better to maintain silence.
Modulate your voice, avoid a monotonic voice, and use pauses. 
Be energetic, smile, and self-confident, up to a little bit unnatural level for you. This will inspire the audience.

Prepare yourself for possible questions. For this, you can use the bucket strategy. Think of a list of the most probable questions. Classify them in buckets based on a keyword. Prepare the best generic answer for each category and rehearse them. In the Q&A section, listen carefully to the questions and quickly find the keyword. Go to the bucket and pick up the generic answer. Say it with self-confidence and maintain eye contact with the person asking it.

How to prepare for your talk?

1. Write your complete talk in the notes part of the slides in PowerPoint. Use full sentences, but just 4-5 of them.
2. Highlight the keywords in each sentence and make a first rehearsal.
3. Leave just the highlighted keywords. Remove the remaining part of the sentences.
4. Remember one main thought from each slide that you would like the audience to remember. The slide content should be connected with this thought.
5. Rehearse the presentation without notes, just using slides.

